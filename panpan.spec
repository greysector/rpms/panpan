%define _enable_debug_packages %{nil}
%define debug_package          %{nil}

Summary: Third person open world adventure and puzzle game
Name: panpan
Version: 2.1.0.3
Release: 1
URL: https://www.gog.com/game/panpan
Group:  Applications/Games
Source0: https://cdn-hw.gog.com/secure/panpan/linux/gog_pan_pan_2.1.0.3.sh
Source1: panpan.desktop
License: GOG
BuildRequires: bsdtar
ExclusiveArch: x86_64
Requires: libpulse-simple.so.0()(64bit)
Requires: libudev.so.1()(64bit)
Requires: mesa-dri-drivers%{_isa}

%global __provides_exclude_from ^%{_libdir}/%{name}/PAN-PAN_Data/(Plugins|Mono)/x86_64/.*\\.so$
%global __requires_exclude ^libsteam_api.so

%description
Might and Delight in collaboration with SPELKRAFT invites you to explore a world
lush with colour and punctuated by the tranquil sounds of a melodic soundtrack.
Welcome to the world of Pan-Pan!

Pan-Pan is an open plain adventure that expands on the concept of environmental
narrative storytelling with puzzle solving and exploration elements at its core.

Set against a backdrop of soothing ambient sounds crafted by renowned sound
composer Simon Viklund, your task is to fix your downed spaceship and begin the
pilgrimage home.

In a world littered with world shifting riddles and sprinkled with the memorable
characters - how you solve these riddles is firmly in your hands.

* Set in an open world: Unlock new areas by collecting items and solving puzzles.
* Stellar soundtrack created by acclaimed sound producer – Simon Viklund.
* A balanced blend of exploration and puzzle solving game mechanics.
* Story beats told through environmental interactions.
* Use a mix of tools, gadgets and devices to solve puzzles and uncover the path
  home.

%prep
%setup -qcT
bsdtar -x -f%{S:0}
find data/noarch/game/PAN-PAN_Data -type f -print0 | xargs -0 chmod -x
chmod +x data/noarch/game/PAN-PAN_Data/{Mono,Plugins}/x86_64/*.so

%install
install -dm755 %{buildroot}%{_libdir}/%{name}
cp -pr data/noarch/game/PAN-PAN_Data %{buildroot}%{_libdir}/%{name}
install -pm755 data/noarch/game/PAN-PAN.x86_64 %{buildroot}%{_libdir}/%{name}

install -Dpm644 data/noarch/support/icon.png %{buildroot}%{_datadir}/icons/hicolor/256x256/apps/%{name}.png
desktop-file-install --dir %{buildroot}%{_datadir}/applications %{S:1}

%files
%license "data/noarch/docs/End User License Agreement.txt"
%{_datadir}/applications/%{name}.desktop
%{_datadir}/icons/hicolor/256x256/apps/%{name}.png
%{_libdir}/%{name}

%changelog
* Sun Dec 27 2020 Dominik Mierzejewski <rpm@greysector.net> 2.1.0.3-1
- initial build
